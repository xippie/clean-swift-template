# Clean Swift Templates v3.0.2
[![Language](https://img.shields.io/badge/swift-4-orange.svg)](https://swift.org)
[![Release](https://img.shields.io/badge/version-3.0.2-blue.svg)](https://gitlab.com/xippie/clean-swift-template)
[![Author](https://img.shields.io/badge/author-rayvinly-green)](https://www.linkedin.com/in/rayvinly)

## Version re-adapted, with 4-tab indent and curly bracket after method signature

To learn more about Clean Swift and the VIP cycle, read:

http://clean-swift.com/clean-swift-ios-architecture

There is a sample app available at:

https://github.com/Clean-Swift/CleanStore

To install the Clean Swift Xcode templates, run:

> make install_templates

To uninstall the Clean Swift Xcode templates, run:

> make uninstall_templates
